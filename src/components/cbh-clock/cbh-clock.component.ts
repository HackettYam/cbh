import { Component, OnInit, Input } from '@angular/core'
import { AngularFireDatabase } from 'angularfire2/database'
import { AngularFireAuth } from 'angularfire2/auth'
import { Observable } from 'rxjs/Observable'

import * as firebase from 'firebase'
import * as moment from 'moment'

@Component({
  selector: 'cbh-clock',
  templateUrl: './cbh-clock.component.html',
  styleUrls: ['./cbh-clock.component.scss']
})
export class CbhClockComponent implements OnInit {
  @Input() public channel: number
  public loading: boolean
  public timeLeft: any
  public cbhItem: any
  public timepicker: any
  public showTimepicker: boolean

  public timepickerOptions: Pickadate.TimeOptions = {
    default: moment().utc().format('HH:mm'), // Set default time: 'now', '1:30AM', '16:30'
    // fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
    twelvehour: false, // Use AM/PM or 24-hour format
    donetext: 'OK', // text for done-button
    cleartext: 'Clear', // text for clear-button
    canceltext: 'Cancel', // Text for cancel-button
    autoclose: true, // automatic close timepicker
    // ampmclickable: true, // make AM PM clickable
    // aftershow: () => alert('AfterShow has been invoked.'), // function for after opening timepicker
  }

  constructor(
    private db: AngularFireDatabase
  ) {
    this.loading = true
    this.showTimepicker = false
    this.timepicker = ``
    firebase.auth().signInAnonymously().catch((err) => {
      console.log(err)
    })
  }

  ngOnInit() {
    const cbhItem = this.db.list(`cbh-clock/ch${this.channel}`)
    const utc = moment().utc()

    cbhItem.valueChanges().subscribe((item) => {
      if (!item || item.length <= 0) {
        this.cbhItem = {
          id: this.channel,
          title: `Channel ${this.channel}`,
          clock: ``,
          datetime: ``
        }

        cbhItem.set(`/`, this.cbhItem)
      } else {
        const _item = {
          id: item[2],
          title: item[3],
          clock: item[0],
          datetime: item[1]
        }
        this.cbhItem = _item
      }

      if (this.cbhItem.clock === ``) {
        this.cbhItem.clock = `--:--:--`
      }

      this.loading = false

      setInterval(() => {
        if (this.cbhItem && this.cbhItem.datetime) {
          this.setTimeLeft(this.cbhItem.datetime)
        }
      }, 10000)
    })
  }

  public onNext() {
    const currentDate = moment().utc().format('YYYY-MM-DD')
    const cbhItem = this.db.list(`cbh-clock/ch${this.channel}`)
    let nextUtc = moment().add(6, 'hours').utc()

    if (this.timepicker && this.timepicker !== ``) {
      const time = this.timepicker.split(':')
      const utc = moment().utc()
      utc.set({hour: time[0], minute: time[1], second: 0})
      utc.add(6, 'hours')
      // nextUtc = moment(`${currentDate} ${this.timepicker}`)
      nextUtc = utc
    }

    this.cbhItem.clock = nextUtc.format('HH:mm:ss')
    this.cbhItem.datetime = nextUtc.format()
    cbhItem.set(`/`, this.cbhItem)

    this.showTimepicker = false
    this.timepicker = ``
  }

  public hasTimeLeft() {
    const utc = moment().utc()
    const _utc = moment(this.cbhItem.datetime).utc()
    return _utc > utc
  }

  public setTimeLeft(datetime) {
    const _utc = moment(datetime).utc()
    this.timeLeft = _utc.endOf('minute').fromNow()
  }

  public getTimeLeft() {
    if (this.hasTimeLeft()) {
      const _utc = moment(this.cbhItem.datetime).utc()
      this.timeLeft = _utc.endOf('minute').fromNow()
    }
    return this.timeLeft
  }

  public toggleSettings() {
    this.showTimepicker = !this.showTimepicker
    if (!this.showTimepicker) { this.timepicker = `` }
  }
}
