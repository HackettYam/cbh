import { Component, OnInit, Input } from '@angular/core'

import * as moment from 'moment'

@Component({
  selector: 'clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss']
})
export class ClockComponent implements OnInit {
  public clock: any

  constructor() { }

  ngOnInit() {
    setInterval(() => {
      this.clock = moment().utc().format(`HH:mm:ss`)
    }, 1000)
  }

}
