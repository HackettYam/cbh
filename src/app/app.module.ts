import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { AngularFireModule } from 'angularfire2'
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database'
import { AngularFireAuthModule } from 'angularfire2/auth'

import 'hammerjs'
import 'materialize-css'

import { MaterializeModule } from 'ng2-materialize'
import { firebaseConfig } from './../config/firebase.config'

import { AppRoutingModule } from './app.routing'
import { AppComponent } from './app.component'
import { HeaderComponent } from './../components/header/header.component'
import { SidenavComponent } from './../components/sidenav/sidenav.component'
import { ClockComponent } from '../components/clock/clock.component'
import { CbhClockComponent } from '../components/cbh-clock/cbh-clock.component'
import { CbhPage } from '../pages/cbh/cbh.page'


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidenavComponent,
    ClockComponent,
    CbhClockComponent,
    CbhPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    AppRoutingModule,
    MaterializeModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  exports: [
  ],
  providers: [
    AngularFireDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
