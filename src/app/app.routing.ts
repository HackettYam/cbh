import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { CbhPage } from '../pages/cbh/cbh.page'

const appRoutes: Routes = [
  { path: 'cbh',        component: CbhPage },
  { path: '',   redirectTo: '/cbh', pathMatch: 'full' },
  // { path: '**', component: PageNotFoundComponent }
]

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
