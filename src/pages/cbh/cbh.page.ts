import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-cbh',
  templateUrl: './cbh.page.html',
  styleUrls: ['./cbh.page.scss']
})
export class CbhPage implements OnInit {
  public title: string

  constructor() {
    this.title = `Cold-blooded Heretic`
  }

  ngOnInit() { }

}
